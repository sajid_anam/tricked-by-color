package com.wub.sajid.trickedbycolor;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class HowTo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.how_to);

        TextView howTo = (TextView) findViewById(R.id.howto);
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");

        howTo.setTypeface(tf);
    }
}
