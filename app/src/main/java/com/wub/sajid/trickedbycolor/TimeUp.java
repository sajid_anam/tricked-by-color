package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class TimeUp extends AppCompatActivity {

    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.time_up);

        mPrefs = getSharedPreferences("Data", Context.MODE_PRIVATE);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        TextView status = (TextView) findViewById(R.id.status);

        Integer flag = getIntent().getFlags();
        Integer highScore = 0;

        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.load(TimeUp.this);
        }

        if (flag == 1) {
            highScore = mPrefs.getInt("EasyLevelHighScore", 0);
            status.setText("Time Up");
            status.setTextColor(Color.parseColor("#33B5E5"));
        } else if (flag == 2) {
            highScore = mPrefs.getInt("MediumLevelHighScore", 0);
            status.setText("Time Up");
            status.setTextColor(Color.parseColor("#33B5E5"));
        } else if (flag == 3) {
            highScore = mPrefs.getInt("HardLevelHighScore", 0);
            status.setText("Time Up");
            status.setTextColor(Color.parseColor("#33B5E5"));
        } else if (flag == 4) {
            highScore = mPrefs.getInt("EasyLevelHighScore", 0);
            status.setText("Game Over");
            status.setTextColor(Color.parseColor("#EE1A19"));
        } else if (flag == 5) {
            highScore = mPrefs.getInt("MediumLevelHighScore", 0);
            status.setText("Game Over");
            status.setTextColor(Color.parseColor("#EE1A19"));
        } else if (flag == 6) {
            highScore = mPrefs.getInt("HardLevelHighScore", 0);
            status.setText("Game Over");
            status.setTextColor(Color.parseColor("#EE1A19"));
        }

        Integer score = getIntent().getExtras().getInt("score");

        TextView bestScore = (TextView) findViewById(R.id.bestscore);
        TextView finalScore = (TextView) findViewById(R.id.score);
        TextView scr = (TextView) findViewById(R.id.scr);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");
        finalScore.setTypeface(tf);
        bestScore.setTypeface(tf);
        scr.setTypeface(tf);
        status.setTypeface(tf);

        finalScore.setText("" + score);
        bestScore.setText("Best " + highScore);
    }

    @Override
    public void onBackPressed() {
        Intent back = new Intent(TimeUp.this, LevelOptions.class);
        startActivity(back);
        finish();
    }

    public void StartAgain(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }
        int flag = getIntent().getFlags();

        if ((flag == 1) || (flag == 4)) {
            Intent intent = new Intent(TimeUp.this, EasyLevel.class);
            startActivity(intent);
            finish();
        } else if ((flag == 2) || (flag == 5)) {
            Intent intent = new Intent(TimeUp.this, MediumLevel.class);
            startActivity(intent);
            finish();
        } else if ((flag == 3) || (flag == 6)) {
            Intent intent = new Intent(TimeUp.this, HardLevel.class);
            startActivity(intent);
            finish();
        }
    }

    public void goHome(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }
        Intent intent = new Intent(TimeUp.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
