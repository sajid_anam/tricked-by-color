package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Highscore extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.high_score);

        SharedPreferences mPrefs = getSharedPreferences("Data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPrefs.edit();

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        final Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");

        TextView easy = (TextView) findViewById(R.id.easy);
        TextView medium = (TextView) findViewById(R.id.medium);
        TextView hard = (TextView) findViewById(R.id.hard);
        TextView average = (TextView) findViewById(R.id.average);
        TextView topView = (TextView) findViewById(R.id.topview);

        easy.setTypeface(tf);
        medium.setTypeface(tf);
        hard.setTypeface(tf);
        topView.setTypeface(tf);

        Integer score_easy = mPrefs.getInt("EasyLevelHighScore", 0);
        Integer score_medium = mPrefs.getInt("MediumLevelHighScore", 0);
        Integer score_hard = mPrefs.getInt("HardLevelHighScore", 0);

        easy.setText("Easy: " + score_easy);
        medium.setText("Medium: " + score_medium);
        hard.setText("Hard: " + score_hard);

        Float score_average = (float) (score_easy + score_medium + score_hard) / 3;

        editor.putFloat("AverageScore", score_average);
        editor.apply();

        average.setText("Average " + score_average);
        average.setTypeface(tf);
    }
}
