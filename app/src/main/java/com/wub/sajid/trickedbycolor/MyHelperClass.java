package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;


public class MyHelperClass {
    private static SoundPool sp;
    private static int buttonSound;

    public static void load(Context context) {
        sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        buttonSound = sp.load(context, R.raw.keypress, 1);
    }

    public static void play() {
        sp.play(buttonSound, 1, 1, 0, 0, 1);
    }

    public static int generateRandomColor(int limit) {
        return (int) (Math.random() * limit);
    }

    public static int generateRandomText(int limit, int lastUsed) {
        int randomNumber = (int) (Math.random() * limit);

        while (randomNumber == lastUsed) {
            randomNumber = (int) (Math.random() * limit);
        }
        return randomNumber;
    }
}
