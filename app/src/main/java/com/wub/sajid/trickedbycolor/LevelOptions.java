package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class LevelOptions extends AppCompatActivity {

    private Intent intent;
    private SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.level_options);

        mPrefs = getSharedPreferences("Data", Context.MODE_PRIVATE);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");

        TextView easy = (TextView) findViewById(R.id.easy);
        TextView medium = (TextView) findViewById(R.id.medium);
        TextView hard = (TextView) findViewById(R.id.hard);

        easy.setTypeface(tf);
        medium.setTypeface(tf);
        hard.setTypeface(tf);

        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.load(LevelOptions.this);
        }
    }

    @Override
    public void onBackPressed() {
        intent = new Intent(LevelOptions.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void StartLevel(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        int id = view.getId();

        if (id == R.id.easy) {
            intent = new Intent(LevelOptions.this, EasyLevel.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.medium) {
            intent = new Intent(LevelOptions.this, MediumLevel.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.hard) {
            intent = new Intent(LevelOptions.this, HardLevel.class);
            startActivity(intent);
            finish();
        }
    }
}
