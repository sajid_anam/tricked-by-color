package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;

import java.util.ArrayList;
import java.util.Collections;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class EasyLevel extends AppCompatActivity {

    private TextView color;
    private TextView point;
    private TextView timerView;
    private int score = 0;
    private String[] colorNames;
    private CountDownTimer ct;
    private int[] androidColors;
    private int v;
    private Vibrator vibrator;
    private ArrayList<Integer> objects = new ArrayList<>();
    private ArrayList<ImageButton> buttons = new ArrayList<>();
    private int flag = 1;
    private long leftTime;
    private Intent intent;
    private ImageView pauseButton;
    private GoogleApiClient mGoogleApiClient;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.easy_level);

        mPrefs = getSharedPreferences("Data", Context.MODE_PRIVATE);
        editor = mPrefs.edit();

        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.load(EasyLevel.this);
        }

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();
        mGoogleApiClient.connect();

        intent = new Intent(EasyLevel.this, TimeUp.class);
        final Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");

        objects.add(R.drawable.red);
        objects.add(R.drawable.blue);
        objects.add(R.drawable.green);
        objects.add(R.drawable.yellow);

        Collections.shuffle(objects);

        buttons.add((ImageButton) findViewById(R.id.b1));
        buttons.add((ImageButton) findViewById(R.id.b2));
        buttons.add((ImageButton) findViewById(R.id.b3));
        buttons.add((ImageButton) findViewById(R.id.b4));

        for (int i = 0; i < objects.size(); i++) {
            buttons.get(i).setBackgroundResource(objects.get(i));
            buttons.get(i).setTag(objects.get(i));
        }

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        color = (TextView) findViewById(R.id.color);
        point = (TextView) findViewById(R.id.point);
        timerView = (TextView) findViewById(R.id.timer);

        color.setTypeface(tf);
        point.setTypeface(tf);
        timerView.setTypeface(tf);

        v = MyHelperClass.generateRandomText(4, 9);
        int z = MyHelperClass.generateRandomColor(4);

        androidColors = getResources().getIntArray(R.array.colorlistEasy);
        colorNames = getResources().getStringArray(R.array.colornameEasy);

        while (v == z) {
            z = MyHelperClass.generateRandomColor(4);
        }

        color.setText(colorNames[v]);
        color.setTextColor(androidColors[z]);

        point.setText("Score: " + String.valueOf(score));

        ct = new CountDownTimer(21000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                timerView.setText("Time: " + millisUntilFinished / 1000);
                leftTime = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                intent.setFlags(1);
                intent.putExtra("score", score);

                if (score > mPrefs.getInt("EasyLevelHighScore", 0)) {

                    editor.putInt("EasyLevelHighScore", score);
                    editor.apply();

                    startActivity(intent);
                    finish();
                } else {
                    startActivity(intent);
                    finish();
                }
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        Intent level = new Intent(EasyLevel.this, LevelOptions.class);
        startActivity(level);
        ct.cancel();
        finish();
    }

    public void rightButtonClicked() {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        int t = MyHelperClass.generateRandomText(4, v);
        v = t;
        int c = MyHelperClass.generateRandomColor(4);

        while (t == c) {
            c = MyHelperClass.generateRandomColor(4);
        }

        color.setText(colorNames[t]);
        color.setTextColor(androidColors[c]);

        score = score + 1;
        point.setText("Score: " + String.valueOf(score));

        if (score == 15) {

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {

                Games.Achievements.unlock(mGoogleApiClient, getResources().getString(R.string.achievement_easy_peasy));
            }
        }

        if (score == 30) {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                Games.Achievements.unlock(mGoogleApiClient, getResources().getString(R.string.achievement_really_easy));
            }
        }
    }

    public void wrongButtonClicked() {
        vibrator.vibrate(400);
        intent.setFlags(4);
        intent.putExtra("score", score);

        if (score > mPrefs.getInt("EasyLevelHighScore", 0)) {
            editor.putInt("EasyLevelHighScore", score);
            editor.apply();

            startActivity(intent);
            ct.cancel();
            finish();
        } else {
            startActivity(intent);
            ct.cancel();
            finish();
        }
    }

    public void onButtonClick(View view) {
        if (color.getText().toString().equals("RED")) {
            if (Integer.parseInt(view.getTag().toString()) == R.drawable.red) {
                rightButtonClicked();
            } else {
                wrongButtonClicked();
            }
        } else if (color.getText().toString().equals("BLUE")) {
            if (Integer.parseInt(view.getTag().toString()) == R.drawable.blue) {
                rightButtonClicked();
            } else {
                wrongButtonClicked();
            }
        } else if (color.getText().toString().equals("GREEN")) {
            if (Integer.parseInt(view.getTag().toString()) == R.drawable.green) {
                rightButtonClicked();
            } else {
                wrongButtonClicked();
            }
        } else if (color.getText().toString().equals("YELLOW")) {
            if (Integer.parseInt(view.getTag().toString()) == R.drawable.yellow) {
                rightButtonClicked();
            } else {
                wrongButtonClicked();
            }
        }
    }

    public void buttonClickable(boolean x) {
        ImageButton b1 = (ImageButton) findViewById(R.id.b1);
        ImageButton b2 = (ImageButton) findViewById(R.id.b2);
        ImageButton b3 = (ImageButton) findViewById(R.id.b3);
        ImageButton b4 = (ImageButton) findViewById(R.id.b4);

        if (x) {
            b1.setClickable(true);
            b2.setClickable(true);
            b3.setClickable(true);
            b4.setClickable(true);
        } else {
            b1.setClickable(false);
            b2.setClickable(false);
            b3.setClickable(false);
            b4.setClickable(false);
        }
    }


    public void pausePlay(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }
        pauseButton = (ImageView) findViewById(R.id.pause);

        if (flag == 1) {
            pauseButton.setImageResource(R.drawable.play);
            ct.cancel();
            flag = 0;

            buttonClickable(false);
        } else if (flag == 0) {
            pauseButton.setImageResource(R.drawable.pause);
            buttonClickable(true);
            ct = new CountDownTimer(leftTime, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timerView.setText("Time: " + millisUntilFinished / 1000);
                    leftTime = millisUntilFinished;
                }

                @Override
                public void onFinish() {
                    Intent intent = new Intent(EasyLevel.this, TimeUp.class);
                    intent.setFlags(1);
                    intent.putExtra("score", score);

                    if (score > mPrefs.getInt("EasyLevelHighScore", 0)) {
                        editor.putInt("EasyLevelHighScore", score);
                        editor.apply();

                        startActivity(intent);
                        finish();
                    } else {
                        startActivity(intent);
                        finish();
                    }
                }
            }.start();
            flag = 1;
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

        pauseButton = (ImageView) findViewById(R.id.pause);
        pauseButton.setImageResource(R.drawable.play);
        ct.cancel();
        flag = 0;

        buttonClickable(false);
    }
}
