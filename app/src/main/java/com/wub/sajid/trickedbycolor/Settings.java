package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class Settings extends AppCompatActivity {

    private TextView sound;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.settings);

        mPrefs = getSharedPreferences("Data", Context.MODE_PRIVATE);
        editor = mPrefs.edit();

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");

        sound = (TextView) findViewById(R.id.sound);
        TextView howTo = (TextView) findViewById(R.id.howto);
        TextView rateMe = (TextView) findViewById(R.id.rateme);
        TextView settings = (TextView) findViewById(R.id.settings);

        sound.setTypeface(tf);
        howTo.setTypeface(tf);
        rateMe.setTypeface(tf);
        settings.setTypeface(tf);

        if (mPrefs.getInt("SoundPref", 1) == 1) {
            sound.setText("Sound: ON");
        } else if (mPrefs.getInt("SoundPref", 1) == 0) {
            sound.setText("Sound: OFF");
        }
    }

    public void toggleSound(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 0) {
            editor.putInt("SoundPref", 1);
            editor.apply();
            sound.setText("Sound: ON");

        } else if (mPrefs.getInt("SoundPref", 1) == 1) {
            editor.putInt("SoundPref", 0);
            editor.apply();
            sound.setText("Sound: OFF");
        }
    }

    public void howTo(View view) {
        Intent intent = new Intent(Settings.this, HowTo.class);
        startActivity(intent);
    }

    public void rateMe(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=com.wub.sajid.trickedbycolor"));
        startActivity(intent);
    }
}
