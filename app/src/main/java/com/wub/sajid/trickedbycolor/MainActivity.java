package com.wub.sajid.trickedbycolor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.BaseGameUtils;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    boolean doubleBackPressedToExit = false;
    private GoogleApiClient mGoogleApiClient;

    private static int RC_SIGN_IN = 9001;

    private boolean mResolvingConnectionFailure = false;
    private boolean mAutoStartSignInflow = true;
    private boolean mSignInClicked = false;

    boolean mExplicitSignOut;
    boolean mInSignInFlow = false;
    private String LEADERBOARD_ID;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main_activity);

        mPrefs = getSharedPreferences("Data", Context.MODE_PRIVATE);
        editor = mPrefs.edit();

        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.load(MainActivity.this);
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();

        LEADERBOARD_ID = getResources().getString(R.string.leaderboard_average_score);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/myfont.ttf");

        TextView start = (TextView) findViewById(R.id.start);
        start.setTypeface(tf);

//        findViewById(R.id.sign_in_button).setOnClickListener(this);
//        findViewById(R.id.sign_out_button).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackPressedToExit) {
            super.onBackPressed();
            return;
        }

        this.doubleBackPressedToExit = true;
        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackPressedToExit = false;
            }
        }, 2000);
    }

    public void startGame(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        Intent intent = new Intent(MainActivity.this, LevelOptions.class);
        startActivity(intent);
        finish();
    }

    public void launchSettings(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        Intent settings = new Intent(MainActivity.this, Settings.class);
        startActivity(settings);
    }

    public void highScore(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        Intent hsore = new Intent(MainActivity.this, Highscore.class);
        startActivity(hsore);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        mExplicitSignOut = mPrefs.getBoolean("mExplicitSignOut", false);
//        if (!mInSignInFlow && !mExplicitSignOut) {
//            // auto sign in
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        long avg = (long) mPrefs.getFloat("AverageScore", 0);

        Games.Leaderboards.submitScore(mGoogleApiClient, LEADERBOARD_ID, avg);
//        findViewById(R.id.sign_in_button).setVisibility(View.GONE);
//        findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingConnectionFailure) {
            return;
        }

        // if the sign-in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow
        if (mSignInClicked || mAutoStartSignInflow) {
            mAutoStartSignInflow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign-in, please try again later."
            if (!BaseGameUtils.resolveConnectionFailure(this,
                    mGoogleApiClient, connectionResult,
                    RC_SIGN_IN, String.valueOf(R.string.signin_other_error))) {
                mResolvingConnectionFailure = false;
            }
        }

        // Put code here to display the sign-in button
    }

    @Override
    public void onConnectionSuspended(int i) {
        // Attempt to reconnect
        mGoogleApiClient.connect();
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (resultCode == RESULT_OK) {
                mGoogleApiClient.connect();
            } else {
                // Bring up an error dialog to alert the user that sign-in
                // failed. The R.string.signin_failure should reference an error
                // string in your strings.xml file that tells the user they
                // could not be signed in, such as "Unable to sign in."
                BaseGameUtils.showActivityResultError(this,
                        requestCode, resultCode, R.string.signin_failure);


                editor.putBoolean("mExplicitSignOut", true);
                editor.apply();
            }
        }
    }


    public void onClick(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }
//        if (view.getId() == R.id.sign_in_button) {
//            // start the asynchronous sign in flow
//            mSignInClicked = true;
//            mGoogleApiClient.connect();
//
//
//            editor.putBoolean("mExplicitSignOut", false);
//            editor.apply();
//
//        } else if (view.getId() == R.id.sign_out_button) {
//
//
//            editor.putBoolean("mExplicitSignOut", true);
//            editor.apply();
//            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//                Games.signOut(mGoogleApiClient);
//                mGoogleApiClient.disconnect();
//
//
//                findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//                findViewById(R.id.sign_out_button).setVisibility(View.GONE);
//            }
//        }
    }

    public void achievements(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            // signed in. Show the "sign out" button and explanation.
            Integer REQUEST_ACHIEVEMENTS = 1;
            startActivityForResult(Games.Achievements.getAchievementsIntent(mGoogleApiClient),
                    REQUEST_ACHIEVEMENTS);
        } else {
            // not signed in. Show the "sign in" button and explanation.
            Toast.makeText(this, "Not signed in", Toast.LENGTH_SHORT).show();
        }
    }

    public void showleaderboard(View view) {
        if (mPrefs.getInt("SoundPref", 1) == 1) {
            MyHelperClass.play();
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Integer REQUEST_LEADERBOARD = 1;
            startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient,
                    LEADERBOARD_ID), REQUEST_LEADERBOARD);
        } else {
            Toast.makeText(this, "Not signed in", Toast.LENGTH_SHORT).show();
        }
    }
}
